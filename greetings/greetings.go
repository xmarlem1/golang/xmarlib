package greetings

import "fmt"

func GetGreeting(name string) string {
	return fmt.Sprintf("Hello %v\n", name)
}
