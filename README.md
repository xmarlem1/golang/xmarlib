# XMARLIB

A simple go library...


```bash
go list -m -versions gitlab.com/xmarlem1/golang/xmarlib
```

```bash
go get -u gitlab.com/xmarlem1/golang/xmarlib@v0.0.4
```



Push a new version of the library:

```bash
git tag v0.0.5
git push origin v0.0.5
```


How to import from another go project:

```go
import (
  xdig "gitlab.com/xmarlem1/golang/xmarlib/dig"
  xmath "gitlab.com/xmarlem1/golang/xmarlib/math"
)
```
