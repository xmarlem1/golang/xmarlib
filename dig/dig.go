package xmarlib

import "fmt"

func DescribeType[T comparable](val T) {
  fmt.Printf("Describe (value, type): (%v, %T)\n", val, val)
}

func DescribeSlice[T comparable](s []T) {
  fmt.Printf("Type: %T, Len: %v, Cap: %v", s, len(s), cap(s))
}

func SayHello() {
  fmt.Println("Hello world")
}
